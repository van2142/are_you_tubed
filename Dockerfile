##FROM debian:10
##FROM apache/airflow:2.0.2-python3.8
FROM apache/airflow:2.2.3

USER root

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=UTC
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt update 
#RUN apt upgrade -y
RUN apt install -y htop sed sudo nano grep libpci-dev keyboard-configuration xfce4 xfce4-goodies xvfb x11-xkb-utils xfonts-100dpi xfonts-75dpi xfonts-scalable xfonts-cyrillic xserver-xorg-core ffmpeg 
RUN sudo apt install -y firefox-esr 

ENV DISPLAY=:99
WORKDIR /root
COPY display.sh .
RUN ls -alh
RUN chmod a+x display.sh
RUN echo "airflow  ALL=(ALL)       NOPASSWD: ALL" > /etc/sudoers

#CMD ["/bin/bash", "display.sh"]

#Old commands below, vozmozhno delete?
#RUN mkdir ~/.vnc 
#RUN touch ~/.vnc/passwd
#RUN x11vnc -storepasswd "van2142" ~/.vnc/passwd
#RUN sh -c 'echo "firefox" >> ~/.bashrc'
#RUN cat ~/.bashrc

#CMD ["xvfb-run", "-a", "-s", "\"-screen 0 1366x1080x24/"", "firefox"]
#RUN sed -i 's/pref("media.autoplay.default", 1);/pref("media.autoplay.default", 0)/g' /*/share/firefox-esr/browser/defaults/preferences/firefox.js
#RUN mkdir -p /root/.mozilla/firefox
#RUN ls -alh /root/.mozilla/firefox
#RUN sed -i 's/pref("media.autoplay.default", 1);/pref("media.autoplay.default", 0)/g' /root/.mozilla/firefox/*.default-esr/prefs.js
#RUN cat /root/.mozilla/firefox/*.default-esr | grep autoplay

#RUN /usr/bin/x11vnc -forever -usepw -create
#RUN sed -i 's/pref("media.autoplay.default", 1);/pref("media.autoplay.default", 0)/g' /root/.mozilla/firefox/*.default-esr/prefs.js
#RUN cat /root/.mozilla/firefox/*.default-esr | grep autoplay

#RUN ls -alh /*/share/firefox-esr/browser/defaults/preferences
#RUN sed -i 's/pref("media.autoplay.default", 1);/pref("media.autoplay.default", 0)/g' /*/share/firefox-esr/browser/defaults/preferences/firefox.js
#RUN mkdir -p /var/run/dbus
#RUN dbus-uuidgen > /var/lib/dbus/machine-id
#RUN dbus-daemon --system
#RUN dbus-daemon --config-file=/usr/share/dbus-1/system.conf --print-address

USER airflow 

WORKDIR /opt/airflow

COPY display.sh .
RUN ls -alh
RUN sudo chmod a+x display.sh
COPY fire.sh .
RUN ls -alh
RUN sudo chmod a+x fire.sh

CMD ["/bin/bash", "/opt/airflow/display.sh"]

